/* error definitions */
#define LOADING_OK 101
#define NOT_VALID_FILETYPE -101
#define CANNOT_OPEN_FILE -102
#define CANNOT_READ_FILE -103

int load_file_to_pixmap(char *filename, Display *display, int screennum,
			   int *witdh, int *height, Pixmap *pixmap);
